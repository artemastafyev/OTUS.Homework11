using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LabyrinthGenerator : MonoBehaviour
{
    [SerializeField] private int height;
    [SerializeField] private int width;
    [SerializeField] private LabyrinthItem labyrinthItemPrefab;
    [SerializeField] private LabyrinthItem[,] map;

    [ContextMenu("Generate")]
    private void Generate()
    {
        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        map = new LabyrinthItem[height, width];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                var labItem = Instantiate(labyrinthItemPrefab,
                    new Vector3(x * 10, 0, y * 10),
                    Quaternion.identity,
                    transform);

                labItem.X = x;
                labItem.Y = y;

                map[y, x] = labItem;
            }
        }

        //ваша реализация стен для map
        BuildMaze();
    }

    private void BuildMaze()
    {
        LabyrinthItem startCell = map[0, Random.Range(0, width)];
        LabyrinthItem current = startCell;
        current.IsVisited = true;

        Stack<LabyrinthItem> stack = new Stack<LabyrinthItem>();
        do
        {
            List<LabyrinthItem> unvisitedNeibors = new List<LabyrinthItem>();

            if (current.X > 0 && map[current.Y, current.X - 1].IsVisited == false)
                unvisitedNeibors.Add(map[current.Y, current.X - 1]);

            if (current.X < width - 1 && map[current.Y, current.X + 1].IsVisited == false)
                unvisitedNeibors.Add(map[current.Y, current.X + 1]);

            if (current.Y > 0 && map[current.Y - 1, current.X].IsVisited == false)
                unvisitedNeibors.Add(map[current.Y - 1, current.X]);

            if (current.Y < height - 1 && map[current.Y + 1, current.X].IsVisited == false)
                unvisitedNeibors.Add(map[current.Y + 1, current.X]);

            if (unvisitedNeibors.Count > 0)
            {
                LabyrinthItem next = unvisitedNeibors[Random.Range(0, unvisitedNeibors.Count)];
                RemoveWall(current, next);
                next.IsVisited = true;
                current = next;
                stack.Push(next);
            }
            else
            {
                current = stack.Pop();
            }
        }
        while (stack.Count > 0);

        LabyrinthItem endCell = map[height - 1, Random.Range(0, width)];
        endCell.HideWall(Wall.Upper);
        startCell.HideWall(Wall.Lower);
    }

    private static void RemoveWall(LabyrinthItem current, LabyrinthItem next)
    {
        if (current.X < next.X)
        {
            current.HideWall(Wall.Right);
            next.HideWall(Wall.Left);
        }
        else if (current.X > next.X)
        {
            current.HideWall(Wall.Left);
            next.HideWall(Wall.Right);
        }
        else if (current.Y < next.Y)
        {
            current.HideWall(Wall.Upper);
            next.HideWall(Wall.Lower);
        }
        else if (current.Y > next.Y)
        {
            current.HideWall(Wall.Lower);
            next.HideWall(Wall.Upper);
        }
    }
}
