using UnityEngine;

public enum Wall
{
    Upper,
    Lower,
    Left,
    Right
}

public class LabyrinthItem : MonoBehaviour
{
    [SerializeField] private GameObject upperWall;
    [SerializeField] private GameObject rightWall;
    [SerializeField] private GameObject lowerWall;
    [SerializeField] private GameObject leftWall;

    public int X;
    public int Y;
    public bool IsVisited { get; set; } = false;

    public void HideWall(Wall wall)
    {
        GetWall(wall).SetActive(false);
    }

    private GameObject GetWall(Wall wall)
    {
        switch (wall)
        {
            case Wall.Left:
                return leftWall;
            case Wall.Upper:
                return upperWall;
            case Wall.Lower:
                return lowerWall;
            case Wall.Right:
                return rightWall;
            default:
                return default;
        }
    }
}
